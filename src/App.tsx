import React from 'react'
import { HashRouter as Router } from 'react-router-dom'

import * as Context from './contexts'
import * as Page from './components/Pages'

import 'materialize-css'


const App: React.FC = () => {
  return (
    <Router hashType='slash'>
      <Context.AuthProvider>
        <Page.Computed />
      </Context.AuthProvider>
    </Router>
  )
}

export default App