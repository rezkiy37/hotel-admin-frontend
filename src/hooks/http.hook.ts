import { useState, useCallback, useContext } from "react"

import * as Context from '../contexts'

export const useHttp = () => {

  const { logout } = useContext(Context.AuthContext)

  const [loading, toggleLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<any>(null)

  const request = useCallback(
    async (url, method = 'GET', body = null, headers = {}) => {
      try {

        if (body) body = JSON.stringify(body)
        headers['Content-Type'] = 'application/json'

        const response = await fetch(url, {
          method, body, headers
        })


        const data = await response.json()

        if (response.status === 401) {
          logout()
          setErrors(data.message)
          toggleLoading(false)
          return false
        }

        if (!response.ok) {
          throw new Error(data.message || 'Request has failed')
        }

        toggleLoading(false)

        return data

      } catch (e) {
        toggleLoading(false)
        setErrors(e.message)
        throw e
      }
    }, [])


  const clearErrors = useCallback(() => setErrors(null), [])

  return { loading, errors, request, clearErrors }
}