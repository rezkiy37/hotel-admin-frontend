import { ReactNode } from "react";

export interface IAuthContext {
  isReady: boolean | null
  isAuthenticated: boolean
  token: string | null
  userID: string | null
  login: (token: string, userID: string) => void
  logout: () => void | null
}

export interface IAuthProviderProps {
  children: ReactNode
}