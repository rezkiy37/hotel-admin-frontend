import React, { createContext, useState, useCallback, useEffect } from "react"
import { IAuthProviderProps, IAuthContext } from "./types"



const AuthContext = createContext<IAuthContext>({
  isReady: false,
  isAuthenticated: false,
  token: null,
  userID: null,
  login: () => { },
  logout: () => { }
})

export default AuthContext



enum EVars {
  USER_DATA = 'USER_DATA'
}

export const AuthProvider: React.FC<IAuthProviderProps> = ({
  children
}) => {

  const [isReady, setIsReady] = useState<boolean>(false)
  const [isAuthenticated, toggleAuthenticated] = useState<boolean>(false)
  const [token, setToken] = useState<string | null>(null)
  const [userID, setUserID] = useState<string | null>(null)

  const login = useCallback((token, userID) => {
    setToken(token)
    setUserID(userID)

    localStorage.setItem(EVars.USER_DATA, JSON.stringify({ token, userID }))

    toggleAuthenticated(true)
  }, [])

  const logout = useCallback(() => {
    setToken(null)
    setUserID(null)

    localStorage.removeItem(EVars.USER_DATA)

    toggleAuthenticated(false)
  }, [])

  useEffect(() => {
    const fetched: any = localStorage.getItem(EVars.USER_DATA)

    const data = JSON.parse(fetched)

    if (data?.token && data?.userID) {
      login(data.token, data.userID)
    }

    setIsReady(true)
  }, [login])

  return (
    <AuthContext.Provider value={{
      isReady, isAuthenticated, token, userID, login, logout
    }}>
      {children}
    </AuthContext.Provider>
  )
}
