import React, { useEffect, useContext, useState } from 'react'

import * as Context from '../../contexts'
import * as Page from '../../components/Pages'
import * as Comopnent from './components'

import { useMesasge } from '../../hooks/message.hook'
import { useHttp } from '../../hooks/http.hook'

import { config } from '../../config/config'

import { IRoom } from './types'


const HotelsPage: React.FC = () => {

  const { token } = useContext(Context.AuthContext)

  const { loading, request } = useHttp()
  const message = useMesasge()

  const [hotels, setHotels] = useState<any[]>([])

  const [showRooms, toggleShowRooms] = useState<boolean>(false)
  const [rooms, setRooms] = useState<IRoom[]>([])

  const Events = {
    roomsHandler: (rooms: IRoom[]) => {
      toggleShowRooms(!showRooms)
      setRooms(rooms)
    },
    closeHandler: () => {
      toggleShowRooms(false)
    }
  }

  const Callbacks = {
    FetchHotels: async () => {
      try {
        const response = await request(`${config.URL_API}/hotels`, 'GET', null, {
          Authorization: token
        })

        if (response) setHotels(response)
      } catch (e) {
        console.log(e)
        message(e)
      }
    }
  }

  useEffect(() => {
    Callbacks.FetchHotels()
  }, [])


  if (loading) {
    return <Page.Preloader />
  }

  return (
    <div className='page-wrapper'>
      <Comopnent.List
        items={hotels}
        events={Events}
      />

      <Comopnent.RoomsBlock
        active={showRooms}
        rooms={rooms}
        closeHandler={Events.closeHandler}
      />
    </div>
  )
}


export default HotelsPage