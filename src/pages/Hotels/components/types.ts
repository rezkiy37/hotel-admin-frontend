import { IRoom, IHotel } from "../types"

export interface IListProps {
  items: any[]
  events: {
    roomsHandler: (rooms: IRoom[]) => void
  }
}

export interface IItemProps {
  data: IHotel
  events: {
    roomsHandler: (rooms: IRoom[]) => void
  }
}

export interface IEditProps {
  active: boolean
  inputRef: React.MutableRefObject<HTMLInputElement | undefined>
  editHandler: () => void
  data: {
    _id: string
    title: string
    description: string
  }
}
