import { IRoom } from "../../types"

export interface IRoomsBlock {
  active: boolean
  rooms: IRoom[]
  closeHandler: () => void
}

export interface IRoomsListProps {
  rooms: IRoom[]
}

export interface IRoomsItemProps {
  data: IRoom
}

export interface IForm {
  title: string
  description: string
  order: number
  price: number
}

export interface IEditProps {
  active: boolean
  data: IRoom
  inputRef: any
  editHandler: () => void
}