import React, { Fragment } from 'react'

import { RoomItem } from './RoomItem'

import { IRoomsListProps } from './type'


export const RoomsList: React.FC<IRoomsListProps> = ({
  rooms
}) => {

  if (!rooms.length) {
    return (
      <div className='empty-block'>

        <img
          alt='Shirt'
          src={require('../../../../assets/icons/shirt.png')}
        />

        <h2 className='empty-title'>
          There is not rooms here :(
        </h2>
      </div>
    )
  }

  return (
    <Fragment>
      {rooms.map(room => (
        <RoomItem
          key={room._id}
          data={room}
        />
      ))}
    </Fragment>
  )
}