import React from 'react'

import { RoomsList } from './RoomsList'

import { IRoomsBlock } from './type'



export const RoomsBlock: React.FC<IRoomsBlock> = ({
  active,
  rooms,
  closeHandler
}) => {

  const Events = {
    closeHandler: () => {
      closeHandler()
    },
    contentClick: (e: React.MouseEvent) => {
      e.stopPropagation()
    }
  }

  return (
    <aside className={`aside ${active && 'active'}`} onClick={Events.closeHandler}>
      <div className='aside--content' onClick={Events.contentClick}>
        <RoomsList
          rooms={rooms}
        />
      </div>
    </aside>
  )
}