import React, { useRef, useState } from 'react'

import { RoomEdit } from './RoomsEdit'

import { IRoomsItemProps } from './type'



export const RoomItem: React.FC<IRoomsItemProps> = ({
  data
}) => {

  const inputRef = useRef<React.PropsWithRef<HTMLInputElement>>();

  const [isActive, toggleActive] = useState<boolean>(false)

  const Events = {
    editHandler: () => {
      toggleActive(!isActive)

      setTimeout(() => {
        inputRef.current?.focus()
      }, 150)
    }
  }

  return (
    <div className='list-item list-item--dark'>

      <div className='list-content list-content--room'>

        <div className='list-cell list-cell--picture'>
          <img
            alt='Hotel'
            src={require('../../../../assets/icons/hotel.png')}
          />
        </div>

        <div className='list-cell list-cell-column list-parent'>
          <h4 className='list-title'>
            {data.title}
          </h4>

          <p className='list-description'>
            {data.description}
          </p>

          <p className='list-description-modal'>
            {data.description}
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Order
          </h4>

          <p className='list-cell-description'>
            {data.order}
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Price
          </h4>

          <p className='list-cell-description'>
            {data.price}
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Last update
          </h4>

          <p className='list-cell-description'>
            {new Date(data.updatedAt).toLocaleString()}
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Last update
          </h4>

          <p className='list-cell-description'>
            {new Date(data.createdAt).toLocaleString()}
          </p>
        </div>

      </div>

      <RoomEdit
        active={isActive}
        data={data}
        editHandler={Events.editHandler}
        inputRef={inputRef}
      />
    </div>
  )
}