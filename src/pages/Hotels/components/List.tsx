import React, { useEffect } from 'react'

import { IListProps } from './types'
import { Item } from './Item'


export const List: React.FC<IListProps> = ({
  items,
  events
}) => {

  useEffect(() => {
    const list = document.querySelector('.page-list')
    list?.classList.add('active')
  }, [])


  return (
    <div className='page-list'>
      {items.map(item => (
        <Item
          key={item._id}
          data={item}
          events={events}
        />
      ))}
    </div>
  )
}