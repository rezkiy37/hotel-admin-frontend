import React, { useState, useEffect, useContext } from 'react'

import * as Context from '../../../contexts'
import * as Form from '../../../components/Form'

import { useHttp } from '../../../hooks/http.hook'
import { useMesasge } from '../../../hooks/message.hook'

import { IEditProps } from './types'
import { IForm } from '../types'
import { config } from '../../../config/config'


export const Edit: React.FC<IEditProps> = ({
  active,
  inputRef,
  data,
  editHandler
}) => {

  const { token } = useContext(Context.AuthContext)

  const { loading, request } = useHttp()
  const message = useMesasge()

  const [isFirstRender, setIsFirstRender] = useState<boolean>(true)
  const [isTitle, toggleIsTitle] = useState<boolean>(true)
  const [IsDescription, toggleIsDescription] = useState<boolean>(true)

  const [form, setForm] = useState<IForm>({
    title: data.title,
    description: data.description
  })

  const Events = {
    inputHandler: (e: React.ChangeEvent<HTMLInputElement>) => {
      setIsFirstRender(false)
      setForm({ ...form, [e.target.name]: e.target.value })
    },
    pressHandler: (e: React.KeyboardEvent) => {
      if (e.key === 'Enter') Callbacks.Save()
    },
    saveHandler: () => {
      Callbacks.Save()
    },
    deleteHandler: () => {
      Callbacks.Delete()
    }
  }

  const Callbacks = {
    Save: async () => {
      try {
        const response: any = await request(`${config.URL_API}/hotels/${data._id}`, 'POST', { ...form }, {
          Authorization: token
        })
        if (response) {
          await window.location.reload(true)
          message('Hotel was updated!')
        }
      } catch (e) {
        console.log(e)
        message(e)
      }
    },
    Delete: async () => {
      try {
        const response: any = await request(`${config.URL_API}/hotels/${data._id}`, 'DELETE', null, {
          Authorization: token
        })
        if (response) {
          await window.location.reload(true)
          message('Hotel was deleted!')
        }
      } catch (e) {
        console.log(e)
        message(e)
      }
    },
  }

  useEffect(() => {
    if (!isFirstRender) {
      if (form.title.length >= 2) {
        toggleIsTitle(true)
      } else {
        toggleIsTitle(false)
      }
    }
  }, [form.title])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.description.length >= 4) {
        toggleIsDescription(true)
      } else {
        toggleIsDescription(false)
      }
    }
  }, [form.description])


  return (
    <div className='list-edit'>

      <div className={`list-edit-content ${active && 'active'}`}>
        <div className='list-edit-container'>

          <Form.Input
            name='title'
            type='text'
            placeholder='Change hotel title'
            color='blue'
            isValid={isTitle}
            value={form.title}
            events={Events}
            inputRef={inputRef}
            isActiveLabel={true}
          />

          <Form.Input
            name='description'
            type='text'
            placeholder='Change hotel description'
            color='blue'
            isValid={IsDescription}
            value={form.description}
            events={Events}
            isActiveLabel={true}
          />

          <div className='list-edit-row'>
            <button
              className='list-edit-btn list-edit-btn--save'
              onClick={Events.saveHandler}
            >
              Save
            </button>

            <button
              className='list-edit-btn list-edit-btn--delete'
              onClick={Events.deleteHandler}
            >
              Delete
            </button>
          </div>

        </div>
      </div>

      <button
        className={`list-edit-toggler ${active ? 'open' : 'close'}`}
        disabled={loading}
        onClick={editHandler}
      >
        <img src={require('../../../assets/icons/arrow.png')} />
      </button>
    </div>
  )
}