import React, { useState, useRef } from 'react'

import { Edit } from './Edit'

import { IItemProps } from './types'


export const Item: React.FC<IItemProps> = ({
  data,
  events
}) => {

  const inputRef = useRef<React.PropsWithRef<HTMLInputElement>>();

  const [isActive, toggleActive] = useState<boolean>(false)

  const Events = {
    roomsHandler: () => {
      events.roomsHandler(data.rooms)
    },
    editHandler: () => {
      toggleActive(!isActive)

      setTimeout(() => {
        inputRef.current?.focus()
      }, 150)
    }
  }

  return (
    <div className='list-item'>

      <div className='list-content'>

        <div className='list-cell list-cell--picture'>
          <img
            alt='Hotel'
            src={require('../../../assets/icons/hotel.png')}
          />
        </div>

        <div className='list-cell list-cell-column list-parent'>
          <h4 className='list-title'>
            {data.title}
          </h4>

          <p className='list-description'>
            {data.description}
          </p>

          <p className='list-description-modal list-description-modal--dark'>
            {data.description}
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Rooms count
        </h4>

          <p className='list-cell-description'>
            <span>
              {data.rooms.length}
            </span>

            <button
              className='list-btn list-btn--more'
              onClick={Events.roomsHandler}
            >
              Show all rooms
          </button>
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Last update
        </h4>

          <p className='list-cell-description'>
            {new Date(data.updatedAt).toLocaleString()}
          </p>
        </div>

        <div className='list-cell list-cell-column'>
          <h4 className='list-cell-title'>
            Last update
        </h4>

          <p className='list-cell-description'>
            {new Date(data.createdAt).toLocaleString()}
          </p>
        </div>

      </div>


      <Edit
        active={isActive}
        inputRef={inputRef}
        data={data}
        editHandler={Events.editHandler}
      />

    </div>
  )
}