export interface IForm {
  title: string
  description: string
}

export interface IHotel {
  _id: string
  title: string
  description: string
  rooms: IRoom[]
  createdAt: string
  updatedAt: string
}

export interface IRoom {
  _id: string
  order: number
  title: string
  description: string
  price: number
  hotel: string
  createdAt: string
  updatedAt: string
}