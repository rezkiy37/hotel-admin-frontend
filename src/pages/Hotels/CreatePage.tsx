import React, { useState, useEffect, useContext } from 'react'

import * as Context from '../../contexts'
import * as Page from '../../components/Pages'
import * as Form from '../../components/Form'

import { useHttp } from '../../hooks/http.hook'
import { useMesasge } from '../../hooks/message.hook'

import { IForm } from './types'
import { config } from '../../config/config'



const CreatePage: React.FC = () => {

  const { token } = useContext(Context.AuthContext)

  const { loading, request } = useHttp()
  const message = useMesasge()

  const [isFirstRender, setIsFirstRender] = useState<boolean>(true)
  const [isTitle, toggleIsTitle] = useState<boolean>(true)
  const [IsDescription, toggleIsDescription] = useState<boolean>(true)

  const [form, setForm] = useState<IForm>({
    title: '',
    description: ''
  })


  const Events = {
    inputHandler: (e: React.ChangeEvent<HTMLInputElement>) => {
      setIsFirstRender(false)
      setForm({ ...form, [e.target.name]: e.target.value })
    },
    pressHandler: (e: React.KeyboardEvent) => {
      if (e.key === 'Enter') Callbacks.createHandler()
    }
  }

  const Callbacks = {
    createHandler: async () => {
      if (!isFirstRender && isTitle && IsDescription) {
        try {
          const response = await request(`${config.URL_API}/hotels/create`, 'POST', { ...form }, {
            Authorization: token
          })

          if (response) {
            setForm({ title: '', description: '' })
            message('Hotel was created!')
          }

        } catch (e) {
          message(e)
          console.log(e)
        }
      } else {
        message('Uncorrect data')
      }
    }
  }


  useEffect(() => {
    const card = document.querySelector('.page-card')
    card?.classList.add('active')
  }, [])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.title.length >= 2) {
        toggleIsTitle(true)
      } else {
        toggleIsTitle(false)
      }
    }
  }, [form.title])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.description.length >= 4) {
        toggleIsDescription(true)
      } else {
        toggleIsDescription(false)
      }
    }
  }, [form.description])


  if (loading) {
    return <Page.Preloader />
  }

  return (
    <div className='page-wrapper'>
      <div className='page-card' >

        <Form.Input
          name='title'
          placeholder='Hotel title'
          type='text'
          isValid={isTitle}
          value={form.title}
          events={Events}
          color='blue'
          style={{
            marginBottom: 30
          }}
        />

        <Form.Input
          name='description'
          placeholder='Hotel description'
          type='text'
          isValid={isTitle}
          value={form.description}
          events={Events}
          color='blue'
        />

        <Form.Button
          title='Create'
          type='create'
          disabled={loading}
          clickHandler={Callbacks.createHandler}
        />

      </div>
    </div>
  )
}


export default CreatePage