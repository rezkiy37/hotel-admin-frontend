import React, { useState } from 'react'

import * as Component from './components'


const AuthPage: React.FC = () => {

  const [isLogin, toggleLogin] = useState<boolean>(true)

  return (
    <section className='app-container app-container--auth'>
      <div className='auth-container'>

        <Component.Header isLogin={isLogin} toggleLogin={toggleLogin} />

        {isLogin ? (
          <Component.Login />
        ) : (
            <Component.Register />
          )}

      </div>
    </section>
  )
}

export default AuthPage