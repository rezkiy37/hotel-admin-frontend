import { Dispatch, SetStateAction } from "react";

export interface ILoginProps {

}

export interface IRegisterProps {

}

export interface IHeaderProps {
  isLogin: boolean
  toggleLogin: Dispatch<SetStateAction<boolean>>
}

export interface IHeaderButton {
  title: string
  active: boolean
  isLogin: boolean
  toggleLogin: Dispatch<SetStateAction<boolean>>
}

export interface ILoginForm {
  email: string
  password: string
}

export interface IRegisterForm extends ILoginForm {
  fullname: string
}

export interface ILoginValid {
  email: boolean
  password: boolean
}
