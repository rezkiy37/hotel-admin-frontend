import React from 'react'

import * as Component from './'

import { IHeaderProps } from './types'


export const Header: React.FC<IHeaderProps> = ({
  isLogin,
  toggleLogin
}) => {

  return (
    <div className='auth-header'>
      <Component.HeaderButton
        title='Login'
        active={isLogin}
        isLogin={isLogin}
        toggleLogin={toggleLogin}
      />

      <Component.HeaderButton
        title='Register'
        active={!isLogin}
        isLogin={isLogin}
        toggleLogin={toggleLogin}
      />
    </div>
  )
}