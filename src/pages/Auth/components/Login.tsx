import React, { useState, Fragment, useContext, useEffect } from 'react'

import * as Form from '../../../components/Form'
import * as Context from '../../../contexts'

import { useHttp } from '../../../hooks/http.hook'
import { useMesasge } from '../../../hooks/message.hook'

import { config } from '../../../config/config'

import { ILoginProps, ILoginForm } from './types'


export const Login: React.FC<ILoginProps> = () => {

  const { login } = useContext(Context.AuthContext)

  const { loading, request } = useHttp()
  const message = useMesasge()

  const [isFirstRender, setIsFirstRender] = useState<boolean>(true)
  const [isEmail, toggleIsEmail] = useState<boolean>(true)
  const [isPassword, toggleIsPassword] = useState<boolean>(true)

  const [form, setForm] = useState<ILoginForm>({
    email: '',
    password: ''
  })

  const Events = {
    inputHandler: (e: React.ChangeEvent<HTMLInputElement>) => {
      setIsFirstRender(false)
      setForm({ ...form, [e.target.name]: e.target.value })
    },
    clickHandler: async () => {
      if (!isFirstRender && isEmail && isPassword) {
        try {
          const response = await request(`${config.URL_API}/auth/login`, 'POST', { ...form })

          if (response?.token && response.userID) {
            message('Login')

            login(response.token, response.userID)
          }

        } catch (e) {
          console.log(e)
        }
      } else {
        message('Enter smth')

      }
    },
    pressHandler: (e: React.KeyboardEvent) => {
      if (e.key === 'Enter') {
        const btn = document.querySelector('.auth-bottom-btn')
        btn?.classList.add('active')

        Events.clickHandler()

        setTimeout(() => {
          btn?.classList.remove('active')
        }, 200)
      }
    }
  }

  useEffect(() => {
    if (!isFirstRender) {
      if (form.email.length >= 4) {
        if (form.email.includes('@')) {
          toggleIsEmail(true)
        } else {
          toggleIsEmail(false)
        }
      } else {
        toggleIsEmail(false)
      }
    }
  }, [form.email])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.password.length >= 6) {
        toggleIsPassword(true)
      } else {
        toggleIsPassword(false)
      }
    }
  }, [form.password])


  return (
    <Fragment>
      <div className='auth-content'>
        <Form.Input
          name='email'
          type='email'
          placeholder='Email'
          value={form.email}
          isValid={isEmail}
          events={Events}
        />

        <Form.Input
          name='password'
          type='password'
          placeholder='Password'
          isValid={isPassword}
          value={form.password}
          events={Events}
        />
      </div>

      <div className='auth-bottom'>
        <Form.Button
          type='auth'
          title='Login'
          disabled={loading}
          clickHandler={Events.clickHandler}
        />
      </div>
    </Fragment>
  )
}