import React from 'react'
import { IHeaderButton } from './types'


export const HeaderButton: React.FC<IHeaderButton> = ({
  title,
  active,
  isLogin,
  toggleLogin
}) => {

  const toggleLoginHandler = () => {
    if (!active) {
      toggleLogin(!isLogin)
    }
  }

  return (
    <button
      className={`auth-header-btn ${active && 'active'}`}
      onClick={toggleLoginHandler}
    >
      {title}
    </button>
  )
}