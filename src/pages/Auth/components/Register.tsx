import React, { useState, Fragment, useEffect } from 'react'

import * as Form from '../../../components/Form'

import { useHttp } from '../../../hooks/http.hook'
import { useMesasge } from '../../../hooks/message.hook'

import { config } from '../../../config/config'

import { IRegisterProps, IRegisterForm } from './types'


export const Register: React.FC<IRegisterProps> = () => {

  const { loading, request } = useHttp()
  const message = useMesasge()


  const [isFirstRender, setIsFirstRender] = useState<boolean>(true)
  const [isFullname, toggleIsFullname] = useState<boolean>(true)
  const [isEmail, toggleIsEmail] = useState<boolean>(true)
  const [isPassword, toggleIsPassword] = useState<boolean>(true)
  const [form, setForm] = useState<IRegisterForm>({
    fullname: '',
    email: '',
    password: ''
  })

  const Events = {
    inputHandler: (e: React.ChangeEvent<HTMLInputElement>) => {
      setIsFirstRender(false)
      setForm({ ...form, [e.target.name]: e.target.value })
    },
    clickHandler: async () => {
      if (!isFirstRender && isFullname && isEmail && isPassword) {
        try {
          const response = await request(`${config.URL_API}/auth/register`, 'POST', { ...form })

          await message(response.message)
        } catch (e) {
          console.log(e)
        }
      } else {
        message('Enter smth')
      }
    },
    pressHandler: (e: React.KeyboardEvent) => {
      if (e.key === 'Enter') {
        const btn = document.querySelector('.auth-bottom-btn')
        btn?.classList.add('active')

        Events.clickHandler()

        setTimeout(() => {
          btn?.classList.remove('active')
        }, 200)
      }
    }
  }

  useEffect(() => {
    if (!isFirstRender) {
      if (form.fullname.length >= 4) {
        toggleIsFullname(true)
      } else {
        toggleIsFullname(false)
      }
    }
  }, [form.fullname])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.email.length >= 4) {
        if (form.email.includes('@')) {
          toggleIsEmail(true)
        } else {
          toggleIsEmail(false)
        }
      } else {
        toggleIsEmail(false)
      }
    }
  }, [form.email])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.password.length >= 6) {
        toggleIsPassword(true)
      } else {
        toggleIsPassword(false)
      }
    }
  }, [form.password])


  return (
    <Fragment>
      <div className='auth-content'>
        <Form.Input
          name='fullname'
          type='text'
          placeholder='Fullname'
          isValid={isFullname}
          value={form.fullname}
          events={Events}
        />

        <Form.Input
          name='email'
          type='email'
          placeholder='Email'
          isValid={isEmail}
          value={form.email}
          events={Events}
        />

        <Form.Input
          name='password'
          type='password'
          placeholder='Password'
          value={form.password}
          isValid={isPassword}
          events={Events}
        />
      </div>

      <div className='auth-bottom'>
        <Form.Button
          type='auth'
          title='Register'
          disabled={loading}
          clickHandler={Events.clickHandler}
        />
      </div>
    </Fragment>
  )
}