import React from 'react'
import { NavLink } from 'react-router-dom'


const NotFound: React.FC = () => {
  return (
    <div>
      404
      <NavLink to='/'>
        go home
      </NavLink>
    </div>
  )
}

export default NotFound