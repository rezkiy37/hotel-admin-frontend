export interface IForm {
  order: number
  title: string
  description: string
  price: number
  hotelID: string
}

export interface IHotels {
  _id: string
  title: string
}