import React, { useState, useEffect, useContext, useRef } from 'react'

import * as Context from '../../contexts'
import * as Form from '../../components/Form'
import * as Page from '../../components/Pages'

import { useHttp } from '../../hooks/http.hook'
import { useMesasge } from '../../hooks/message.hook'

import { IForm, IHotels } from './types'
import { config } from '../../config/config'


const CreatePage: React.FC = () => {

  const { token } = useContext(Context.AuthContext)

  const { loading, request } = useHttp()
  const message = useMesasge()

  const selectRef = useRef<React.PropsWithRef<HTMLSelectElement>>()

  const [isFirstRender, setIsFirstRender] = useState<boolean>(true)
  const [isOrder, toggleIsOrder] = useState<boolean>(true)
  const [isTitle, toggleIsTitle] = useState<boolean>(true)
  const [IsDescription, toggleIsDescription] = useState<boolean>(true)
  const [isPrice, toggleIsPrice] = useState<boolean>(true)
  const [isHotel, toggleIsHotel] = useState<boolean>(true)

  const [hotels, setHotels] = useState<IHotels[]>([])

  const [form, setForm] = useState<IForm>({
    order: 0,
    title: '',
    description: '',
    price: 0,
    hotelID: ''
  })


  const Events = {
    inputHandler: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
      setIsFirstRender(false)
      setForm({ ...form, [e.target.name]: e.target.value })
    },
    pressHandler: (e: React.KeyboardEvent) => {
      if (e.key === 'Enter') Callbacks.createHandler()
    },
  }

  const Callbacks = {
    FetchHotels: async () => {
      try {
        const response = await request(`${config.URL_API}/hotels`, 'GET', null, {
          Authorization: token
        })

        if (response) setHotels(response)
      } catch (e) {
        message(e)
        console.log(e)
      }
    },
    createHandler: async () => {
      if (!isFirstRender && isOrder && isTitle && IsDescription && isPrice && isHotel) {
        try {
          const response = await request(`${config.URL_API}/rooms/create`, 'POST', { ...form }, {
            Authorization: token
          })

          if (response) {
            setForm({ order: 0, title: '', description: '', price: 0, hotelID: '' })
            message('Room was created!')
          }

        } catch (e) {
          message(e)
          console.log(e)
        }
      } else {
        message('Uncorrect data')
      }
    }
  }


  useEffect(() => {
    Callbacks.FetchHotels()

    const card = document.querySelector('.page-card')
    card?.classList.add('active')
  }, [])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.title.length >= 2) {
        toggleIsTitle(true)
      } else {
        toggleIsTitle(false)
      }
    }
  }, [form.title])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.description.length >= 4) {
        toggleIsDescription(true)
      } else {
        toggleIsDescription(false)
      }
    }
  }, [form.description])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.order > 0) {
        toggleIsOrder(true)
      } else {
        toggleIsOrder(false)
      }
    }
  }, [form.order])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.price >= 0) {
        toggleIsPrice(true)
      } else {
        toggleIsPrice(false)
      }
    }
  }, [form.price])

  useEffect(() => {
    if (!isFirstRender) {
      if (form.hotelID) {
        toggleIsHotel(true)
      } else {
        toggleIsHotel(false)
      }
    }
  }, [form.hotelID])


  if (loading) {
    return <Page.Preloader />
  }

  return (
    <div className='page-wrapper'>
      <div className='page-card' >

        <Form.Input
          name='order'
          placeholder='Room order'
          type='number'
          isValid={isOrder}
          value={form.order}
          events={Events}
          color='blue'
          style={{
            marginBottom: 30
          }}
          isActiveLabel={true}
        />

        <Form.Input
          name='title'
          placeholder='Room title'
          type='text'
          isValid={isTitle}
          value={form.title}
          events={Events}
          color='blue'
          style={{
            marginBottom: 30
          }}
        />

        <Form.Input
          name='description'
          placeholder='Room description'
          type='text'
          isValid={isTitle}
          value={form.description}
          events={Events}
          color='blue'
          style={{
            marginBottom: 30
          }}
        />

        <Form.Input
          name='price'
          placeholder='Room price'
          type='number'
          isValid={isPrice}
          value={form.price}
          events={Events}
          color='blue'
          style={{
            marginBottom: 30
          }}
          isActiveLabel={true}
        />

        <Form.Select
          name='hotelID'
          options={hotels}
          value={form.hotelID}
          placeholder='Choose hotel'
          selectHandler={Events.inputHandler}
          selectRef={selectRef}
        />

        <Form.Button
          title='Create'
          type='create'
          disabled={loading}
          clickHandler={Callbacks.createHandler}
        />

      </div>
    </div>
  )
}
export default CreatePage