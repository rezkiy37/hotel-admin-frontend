import React, { Fragment } from 'react'
import { IButtonProps } from './types'



export const Button: React.FC<IButtonProps> = ({
  type,
  title,
  disabled = false,
  overwriteStyles,
  clickHandler
}) => {
  return (
    <Fragment>

      {type === 'auth' && (
        <button
          className='auth-bottom-btn'
          disabled={disabled}
          style={overwriteStyles}
          onClick={clickHandler}
        >
          {title}
        </button>
      )}

      {type === 'create' && (
        <button
          className='page-card-btn'
          disabled={disabled}
          style={overwriteStyles}
          onClick={clickHandler}
        >
          {title}
        </button>
      )}

    </Fragment>
  )
}