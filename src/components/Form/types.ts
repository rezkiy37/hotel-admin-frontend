export interface IButtonProps {
  type: 'auth' | 'create'
  title: string
  disabled?: boolean
  overwriteStyles?: React.CSSProperties
  clickHandler: () => any
}

export interface ICheckBoxProps {

}

export interface IInputProps {
  type: string
  placeholder: string
  name: string
  value: string | number
  isValid?: boolean
  events: {
    inputHandler: ((e: React.ChangeEvent<HTMLInputElement>) => void)
    pressHandler?: (e: React.KeyboardEvent) => void
  }
  style?: React.CSSProperties
  color?: 'white' | 'blue'
  inputRef?: any
  isActiveLabel?: boolean
}

export interface ISelect {
  name: string
  options: ISelectOprion[]
  selectRef?: any
  placeholder?: string
  value: string
  selectHandler: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

interface ISelectOprion {
  title: string
  value?: string
  _id?: string
}

