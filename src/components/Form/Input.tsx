import React, { useEffect } from 'react'

import { IInputProps } from './types'


export const Input: React.FC<IInputProps> = ({
  name,
  type,
  placeholder,
  value,
  isValid,
  events,
  style,
  color = '',
  inputRef = null,
  isActiveLabel = false
}) => {


  useEffect(() => {
    const label = document.getElementById(name + '_label')
    if (isValid) {
      label?.classList.remove('invalid')
    } else {
      label?.classList.add('invalid')
    }
  }, [name, isValid])

  return (
    <div className="input-field col s6">
      <input
        style={style}
        className={`${color} ${!isValid && 'manual-invalid'}`}
        id={name}
        name={name}
        type={type}
        value={value}
        ref={inputRef}
        onChange={events.inputHandler}
        onKeyPress={events.pressHandler || undefined}
      />
      <label
        className={`${color} ${isActiveLabel ? 'active' : ''}`}
        id={name + '_label'}
        htmlFor={name}
      >{placeholder}</label>
    </div>
  )
}