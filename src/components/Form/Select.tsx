import React from 'react'

import { ISelect } from './types'


export const Select: React.FC<ISelect> = ({
  name,
  options = [],
  placeholder,
  selectRef = undefined,
  value,
  selectHandler,
}) => {
  return (
    <select
      name={name}
      value={value}
      onChange={selectHandler}
      ref={selectRef}
    >
      {placeholder ? (
        <option value='' disabled>
          {placeholder}
        </option>
      ) : null}

      {options.map(option => (
        <option key={option._id} value={option.value || option._id}>
          {option.title}
        </option>
      ))}
    </select>
  )
}