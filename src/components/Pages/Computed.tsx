import React, { useContext } from 'react'

import * as Context from '../../contexts'
import * as Component from './'

import Routes from '../../routes/routes'


const Computed: React.FC = () => {

  const { isAuthenticated } = useContext(Context.AuthContext)

  return (
    <section className='app'>
      {isAuthenticated && <Component.Header />}
      <Routes isAuthenticated={isAuthenticated} />
    </section>
  )
}

export default Computed