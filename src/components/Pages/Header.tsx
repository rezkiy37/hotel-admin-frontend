import React, { useContext } from 'react'

import * as Context from '../../contexts'

import { NavLink } from 'react-router-dom'


export const Header: React.FC = () => {

  const { logout } = useContext(Context.AuthContext)

  return (
    <header className='header'>
      <NavLink to='/'>
        <img className='header-img' src={require('../../assets/icons/home.png')} alt='Home' />
      </NavLink>

      <div className='header-box'>
        <NavLink className='header-link' to='/hotels' exact>
          Hotels
        </NavLink>

        <NavLink className='header-link' to='/hotels/create'>
          Create hotel
        </NavLink>

        <NavLink className='header-link' to='/rooms/create'>
          Create room
        </NavLink>

        <button className='header-btn' onClick={() => logout()}>
          Logout
        </button>
      </div>

    </header>
  )
}