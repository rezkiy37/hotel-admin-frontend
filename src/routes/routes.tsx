import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import { IRoutesProps } from './types'

import * as Auth from '../pages/Auth'
import * as Hotels from '../pages/Hotels'
import * as Rooms from '../pages/Rooms'
import * as NotFound from '../pages/NotFound'


const Routes: React.FC<IRoutesProps> = ({
  isAuthenticated
}) => {

  if (isAuthenticated) {
    return (
      <Switch>
        <Route path='/hotels' exact>
          <Hotels.HotelsPage />
        </Route>

        <Route path='/hotels/create' exact>
          <Hotels.CreatePage />
        </Route>

        <Route path='/hotels/:id'>
          <Hotels.DetailPage />
        </Route>

        <Route path='/rooms' exact>
          <Rooms.RoomsPage />
        </Route>

        <Route path='/rooms/create' exact>
          <Rooms.CreatePage />
        </Route>
        <Route path='/rooms/:id'>
          <Rooms.DetailPage />
        </Route>

        <Route path='/404'>
          <NotFound.NotFound />
        </Route>

        <Redirect to='/hotels' />
      </Switch>
    )
  }

  return (
    <Switch>
      <Route path='/'>
        <Auth.AuthPage />
      </Route>

      <Redirect to='/' />
    </Switch>
  )
}

export default Routes